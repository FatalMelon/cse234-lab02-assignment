package com.example.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener {
           rollDice()
        }
        rollDice()

    }

    private fun rollDice() {
        val dice1 = Dice(6)
        val diceRoll1 = dice1.roll()
        val dice2 = Dice(6)
        val diceRoll2 = dice2.roll()

        val dice1Image: ImageView = findViewById(R.id.imageView)
        when(diceRoll1){
            1->dice1Image.setImageResource(R.drawable.dice_1)
            2->dice1Image.setImageResource(R.drawable.dice_2)
            3->dice1Image.setImageResource(R.drawable.dice_3)
            4->dice1Image.setImageResource(R.drawable.dice_4)
            5->dice1Image.setImageResource(R.drawable.dice_5)
            6->dice1Image.setImageResource(R.drawable.dice_6)
        }

        val dice2Image: ImageView = findViewById(R.id.imageView2)
        when(diceRoll2){
            1->dice2Image.setImageResource(R.drawable.dice_1)
            2->dice2Image.setImageResource(R.drawable.dice_2)
            3->dice2Image.setImageResource(R.drawable.dice_3)
            4->dice2Image.setImageResource(R.drawable.dice_4)
            5->dice2Image.setImageResource(R.drawable.dice_5)
            6->dice2Image.setImageResource(R.drawable.dice_6)
        }
    }

    class Dice(private val numSides: Int) {
        fun roll(): Int{
            return (1..numSides).random()
        }
    }
}